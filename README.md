# Overview #

If you want to play with starphleet on a free EC2 tier then this is the script for you!

# Ooo, whats starphleet? #

[Starphleet Documentation](http://wballard.github.io/starphleet/)
[Starphleet Sourcecode](https://github.com/wballard/starphleet)

# Steps #

* Setup a new Free Tier ubuntu machine on EC2
* Ensure ports 80 and 443 are open
* ssh into the new machine
* Run the following command adding your headquarters as a paramater


```
#!bash

sudo bash -c "$(curl -fsSL https://bitbucket.org/Spyro467/starphleet-setup/raw/master/setup)" <<head quaters repo>>
```


* When the script has nearly finished it will generate an ssh key pair - just accept all defaults.
* Copy the generated public ssh key to github/bitbucket - this will be output into the console window so you can copy it
* Run the following to see things are toasty!


```
#!bash

sudo starphleet-logs

```

You can monitor the nginx access.log file in real time:-

```
#!bash

goaccess -f /var/log/upstart/access.log

```